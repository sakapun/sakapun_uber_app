import twitter from "twitter"
import {CreateStoreInput} from "../types/amplify_api";
import {City} from "../src/data/cities";
require("dotenv").config()


const instance = new twitter({
  access_token_key: process.env.TW_ACCESS_TOKEN_KEY as string,
  access_token_secret: process.env.TW_ACCESS_TOKEN_SECRET as string,
  consumer_key: process.env.TW_CONSUMER_API_KEY as string,
  consumer_secret: process.env.TW_CONSUMER_API_SECRET as string,
  // bearer_token: process.env.TW_BEARER_TOKEN,
})

export async function postTweet(appsyncStore: CreateStoreInput, index: number, city: City): Promise<void> {
  const locationUrl = `https://www.google.com/maps/search/?api=1&query=${appsyncStore.location.latitude},${appsyncStore.location.longitude}`
  const tweetString = `ウーバーイーツ${city.city.japanese}の新店：「${appsyncStore.title.substr(0,50)}」 ${locationUrl} `
  await instance.post("statuses/update", {
    status: tweetString
  }).catch(e => console.error(e))
  return
}


