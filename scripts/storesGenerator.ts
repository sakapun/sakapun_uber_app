import {gqlOp} from "../src/lib/amplify-query-helper";
import {listStore2} from "../src/graphql/myquery";
import {listStore2Query, storeFragmentFragment} from "../types/amplify_api";
import Amplify from "aws-amplify";
import awsExports from "../src/aws-exports";
import * as fs from "fs";

// Setting
Amplify.configure(awsExports);

export async function getList (): Promise<storeFragmentFragment[]> {
  const res = await gqlOp<listStore2Query>(listStore2)
  return res.listStores?.items?.reduce<storeFragmentFragment[]>((tmp, store) => {
    if (store) {
      tmp.push(store)
    }
    return tmp;
  }, [] ) || []
}

async function main() {
  const data = await getList()
  await fs.writeFileSync(
    "./src/data/stores.ts",
`import {storeFragmentFragment} from '../../types/amplify_api'
export const storeList: storeFragmentFragment[] = ${JSON.stringify(data, null, "\t")}`
  )
  process.exit(0)
}

main()

