import {LocationStoreMap, StoreDetail} from "../types/uber_type";
import {amplifySignIn, getAllStoreIds, simpleCreateStoreMutation} from "./amplify-cli";
import {postTweet} from "./twitter";
import {addLocationStoresMap, generateAppsyncStores, getStore} from "../src/lib/uber";
import {cities} from "../src/data/cities";


main();
async function main() {
  const CI_CITY_INDEX = process.env.CI_CITY_INDEX ? parseInt(process.env.CI_CITY_INDEX) : 7
  if (CI_CITY_INDEX >= (cities.length)) {
    console.error("array has not index " + CI_CITY_INDEX);
    process.exit(1);
  }

  const city = cities[CI_CITY_INDEX];

  // 検索文字列から対応店舗の取得
  const lsMap: LocationStoreMap = new Map();
  await Promise.all(
    city.searchPoint.map((s, index) => {
      return addLocationStoresMap(s, new Map(), index)
        .then(r => r.forEach((v, k) => {
          lsMap.set(k, v)
        }))
    })
  )

  // 登録済みの全IDを取得する
  const registerdIds = await getAllStoreIds();

  // 未登録店舗があるかどうか
  const storeIds: Set<string> = new Set(Array.from(lsMap.values()).flat().filter(s => !registerdIds.includes(s)))
  if (storeIds.size === 0) {
    // 追加店舗がなかったら終了
    console.log("新規登録店舗はありません")
    process.exit(0)
  }

  // 店舗情報の取得
  const storeMaps: Map<string, StoreDetail> = new Map();
  await Promise.all(Array.from(storeIds.values()).map((s, index) => {
    return getStore(s, index).then(detail => storeMaps.set(detail.uuid, detail))
  }))

  // 最大の配達距離を計測して、appsync登録用に変換する
  const appsyncStores = await generateAppsyncStores(storeMaps, lsMap);



  // appsyncに登録する
  await amplifySignIn().catch(e => console.warn(e));
  await Promise.all(
    appsyncStores.map(simpleCreateStoreMutation)
  )

  // 通知を送る
  await Promise.all(
    appsyncStores.map((store, index) => postTweet(store, index, city))
  )

  process.exit(0)
}
