import {FlatList, Modal, SafeAreaView, Text, View} from "react-native";
import {ListItemStore} from "./ListItemStore";
import React, {useCallback, useMemo, useState} from "react";
import {storeFragmentFragment} from "../../../types/amplify_api";
import {Body, Container, Header, Icon, Left, Right, Title, Item, Input, Button, Form, List} from "native-base";

export type StoreListPageType = {
  sortedStores: storeFragmentFragment[],
  jumpAndCallout: (store: storeFragmentFragment) => void
}
export function StoreListPage (props: StoreListPageType) {
  const {sortedStores, jumpAndCallout} = props

  const {handleChangeText, searchedStores} = useStoreList(sortedStores)

  return (
    <Container>
      <Header searchBar rounded>
        <Left />
        <Body>
          <Title>店舗数({searchedStores.length})</Title>
        </Body>
        <Right />
      </Header>
      <Body>
        <Form>
          <Item regular style={{width: "100%"}}>
            <Input placeholder="Search" onChangeText={handleChangeText} />
          </Item>
        </Form>
        <List
          dataArray={searchedStores}
          renderItem={(data) => {
            return (
              <ListItemStore
                store={data.item}
                handleListClick={jumpAndCallout}
              />
            );
          }}
        />
      </Body>
    </Container>
  )
}

export function useStoreList (stores: storeFragmentFragment[]) {
  const [searchText, setSearch] = useState<string>("")

  const handleChangeText = useCallback((text) => {
    setSearch(text)
  }, [])

  const searchedStores = useMemo(() => {
    return stores.filter(store => store.title.includes(searchText))
  }, [searchText, stores])

  return {
    handleChangeText,
    searchedStores
  }
}
