import {Callout, Marker} from "react-native-maps";
import React, {RefObject, useEffect, useRef} from "react";
import {storeFragmentFragment} from "../../../types/amplify_api";
import {View, Text, Dimensions} from "react-native";
import {MapMarkerRefType} from "../App";

type StorePinType = {
  store: storeFragmentFragment,
  markerRef: RefObject<MapMarkerRefType>
}
export const StorePin = ({store, markerRef}: StorePinType, ) => {

  const thisRef = useRef<Marker | null>(null)
  useEffect(() => {
    if (thisRef.current && markerRef && markerRef.current) {
      markerRef.current.set(store.id, thisRef.current);
    }
  }, [thisRef])

  return (
    <Marker ref={thisRef} key={store.id} coordinate={{
      latitude: store.location.latitude,
      longitude: store.location.longitude
    }}>
      <Callout tooltip={false}>
        <View style={{maxWidth: Dimensions.get('window').width * 0.8}}>
          <Text style={{fontWeight: "bold"}}>{store.title}</Text>
          <Text>{store.location.address}</Text>
        </View>
      </Callout>
    </Marker>
  )
}

