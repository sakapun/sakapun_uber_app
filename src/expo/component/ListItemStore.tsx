import {Text} from "react-native";
import React, {useCallback} from "react";
import {storeFragmentFragment} from "../../../types/amplify_api";
import {ListItem, View} from "native-base";
import {yearMonthDay} from "../../lib/date_format";


export type ListItemStoreType = {
  handleListClick: (store: storeFragmentFragment) => void,
  store: storeFragmentFragment
}
export const ListItemStore = (props : ListItemStoreType) => {
  const {store, handleListClick} = props;
  const handleClick = useCallback(() => {
    handleListClick(store);
  }, [store])

  return (
    <ListItem
      onPress={handleClick}
    >
      <View>
        <Text >{store.title}</Text>
        <Text >登録日：{yearMonthDay(store.updatedAt)}</Text>
      </View>
    </ListItem>
  );
}
