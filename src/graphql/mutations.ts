/* tslint:disable */
/* eslint-disable */
// this is an auto generated file. This will be overwritten

export const createStore = /* GraphQL */ `
  mutation CreateStore(
    $input: CreateStoreInput!
    $condition: ModelStoreConditionInput
  ) {
    createStore(input: $input, condition: $condition) {
      id
      title
      sanitizedTitle
      cityId
      priceBucket
      location {
        address
        streetAddress
        city
        country
        postalCode
        region
        latitude
        longitude
      }
      phoneNumber
      rating {
        ratingValue
        reviewCount
      }
      mostDistance {
        distance
        place
        latitude
        longitude
      }
      createdAt
      updatedAt
    }
  }
`;
export const updateStore = /* GraphQL */ `
  mutation UpdateStore(
    $input: UpdateStoreInput!
    $condition: ModelStoreConditionInput
  ) {
    updateStore(input: $input, condition: $condition) {
      id
      title
      sanitizedTitle
      cityId
      priceBucket
      location {
        address
        streetAddress
        city
        country
        postalCode
        region
        latitude
        longitude
      }
      phoneNumber
      rating {
        ratingValue
        reviewCount
      }
      mostDistance {
        distance
        place
        latitude
        longitude
      }
      createdAt
      updatedAt
    }
  }
`;
export const deleteStore = /* GraphQL */ `
  mutation DeleteStore(
    $input: DeleteStoreInput!
    $condition: ModelStoreConditionInput
  ) {
    deleteStore(input: $input, condition: $condition) {
      id
      title
      sanitizedTitle
      cityId
      priceBucket
      location {
        address
        streetAddress
        city
        country
        postalCode
        region
        latitude
        longitude
      }
      phoneNumber
      rating {
        ratingValue
        reviewCount
      }
      mostDistance {
        distance
        place
        latitude
        longitude
      }
      createdAt
      updatedAt
    }
  }
`;
export const createUbMapNotification = /* GraphQL */ `
  mutation CreateUbMapNotification(
    $input: CreateUBMapNotificationInput!
    $condition: ModelUBMapNotificationConditionInput
  ) {
    createUBMapNotification(input: $input, condition: $condition) {
      expoToken
      cityIds
    }
  }
`;
export const updateUbMapNotification = /* GraphQL */ `
  mutation UpdateUbMapNotification(
    $input: UpdateUBMapNotificationInput!
    $condition: ModelUBMapNotificationConditionInput
  ) {
    updateUBMapNotification(input: $input, condition: $condition) {
      expoToken
      cityIds
    }
  }
`;
export const deleteUbMapNotification = /* GraphQL */ `
  mutation DeleteUbMapNotification(
    $input: DeleteUBMapNotificationInput!
    $condition: ModelUBMapNotificationConditionInput
  ) {
    deleteUBMapNotification(input: $input, condition: $condition) {
      expoToken
      cityIds
    }
  }
`;
