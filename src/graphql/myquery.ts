import gql from "graphql-tag";

export const listStoreIds = gql`
  query ListStoreIds($nextToken: String) {
    listStores(limit: 1000, nextToken: $nextToken) {
      items {
        id
      }
      nextToken
    }
  }
`;

export const LocationFragment = gql`
  fragment Location on Location {
    address
    streetAddress
    city
    country
    postalCode
    region
    latitude
    longitude
    __typename
  }
`;

export const RatingFragment = gql`
  fragment Rating on Rating {
    ratingValue
    reviewCount
    __typename
  }
`;

export const MostDistanceFragment = gql`
  fragment MostDistance on MostDistance {
    distance
    place
    latitude
    longitude
    __typename
  }
`;

export const storeFragment = gql`
  fragment storeFragment on Store {
    id
    title
    sanitizedTitle
    cityId
    priceBucket
    location {
      ...Location
    }
    phoneNumber
    rating {
      ...Rating
    }
    mostDistance {
      ...MostDistance
    }
    createdAt
    updatedAt
    __typename
  }
  ${LocationFragment}
  ${RatingFragment}
  ${MostDistanceFragment}
`;

export const listStore2 = gql`
  query listStore2($nextToken: String) {
    listStores(limit: 1000, nextToken: $nextToken) {
      items {
        ...storeFragment
      }
      nextToken
    }
  }
  ${storeFragment}
`;
