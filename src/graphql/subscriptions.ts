/* tslint:disable */
/* eslint-disable */
// this is an auto generated file. This will be overwritten

export const onCreateStore = /* GraphQL */ `
  subscription OnCreateStore {
    onCreateStore {
      id
      title
      sanitizedTitle
      cityId
      priceBucket
      location {
        address
        streetAddress
        city
        country
        postalCode
        region
        latitude
        longitude
      }
      phoneNumber
      rating {
        ratingValue
        reviewCount
      }
      mostDistance {
        distance
        place
        latitude
        longitude
      }
      createdAt
      updatedAt
    }
  }
`;
export const onUpdateStore = /* GraphQL */ `
  subscription OnUpdateStore {
    onUpdateStore {
      id
      title
      sanitizedTitle
      cityId
      priceBucket
      location {
        address
        streetAddress
        city
        country
        postalCode
        region
        latitude
        longitude
      }
      phoneNumber
      rating {
        ratingValue
        reviewCount
      }
      mostDistance {
        distance
        place
        latitude
        longitude
      }
      createdAt
      updatedAt
    }
  }
`;
export const onDeleteStore = /* GraphQL */ `
  subscription OnDeleteStore {
    onDeleteStore {
      id
      title
      sanitizedTitle
      cityId
      priceBucket
      location {
        address
        streetAddress
        city
        country
        postalCode
        region
        latitude
        longitude
      }
      phoneNumber
      rating {
        ratingValue
        reviewCount
      }
      mostDistance {
        distance
        place
        latitude
        longitude
      }
      createdAt
      updatedAt
    }
  }
`;
export const onCreateUbMapNotification = /* GraphQL */ `
  subscription OnCreateUbMapNotification {
    onCreateUBMapNotification {
      expoToken
      cityIds
    }
  }
`;
export const onUpdateUbMapNotification = /* GraphQL */ `
  subscription OnUpdateUbMapNotification {
    onUpdateUBMapNotification {
      expoToken
      cityIds
    }
  }
`;
export const onDeleteUbMapNotification = /* GraphQL */ `
  subscription OnDeleteUbMapNotification {
    onDeleteUBMapNotification {
      expoToken
      cityIds
    }
  }
`;
