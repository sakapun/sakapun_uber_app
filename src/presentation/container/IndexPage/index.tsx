import React, {useCallback, useEffect, useMemo, useRef, useState} from 'react'
import {
  Grid,
  Box,
  List,
  ListItem,
  Link,
  Button
} from '@chakra-ui/core'
import { Map as LMap, TileLayer, Marker, Popup } from 'react-leaflet'
import 'leaflet/dist/leaflet.css';
import "leaflet/dist/images/marker-shadow.png";
import {useQuery} from "../../../lib/amplify-query-helper";
import {ListStoresQuery, ListStoresQueryVariables} from "../../../../types/amplify_api";
import {listStores} from "../../../graphql/queries";

import L from 'leaflet';
import icon from 'leaflet/dist/images/marker-icon.png';
import iconShadow from 'leaflet/dist/images/marker-shadow.png';
import {ShopNameLabel} from "../../component/ShopNameLabel";
import {LocationStoreMap, StoreDetail} from "../../../../types/uber_type";
import {addLocationStoresMap, getStore} from "../../../lib/uber";

let DefaultIcon = L.icon({
  iconUrl: icon,
  shadowUrl: iconShadow
});

L.Marker.prototype.options.icon = DefaultIcon;

const IndexPageComponent = (props: any) => {
  const center = {
    lat: 37.913209,
    lng: 139.058941
  }
  const {data, loading} = useQuery<ListStoresQuery, ListStoresQueryVariables>(listStores, {
    limit: 500
  })
  const mapRef = useRef<LMap>(null);
  const {stores, findOpenStores} = useOpenStore();
  const genStores = useCallback(() => {
    const storeIds: string[] = data.listStores?.items?.reduce((tmp: string[], s) => {
      if (s) {
        tmp.push(s.id)
      }
      return tmp
    }, []) || []
    findOpenStores(storeIds)
  }, [data.listStores])

  return(
    <Grid
      templateColumns="30% 1fr"
      gap={0}
      height="100%"
      templateRows="100%"
      p={0}
    >
      <Box height={"100%"}>
        <Button onClick={genStores}>開いてる店舗を探す</Button>
        <List height={"100%"} overflowY={"auto"}>
          {!loading &&
            data.listStores?.items
            ?.sort((a,b) => {
              if( !a || !b ) {
                return 1;
              }
              return (b.createdAt || 0) > (a.createdAt || 0) ? 1 : -1;
            })
            .map(store => {
              return !store ? null : (
                <ShopNameLabel title={store.title} lat={store.location.latitude} lng={store.location.longitude} refOb={mapRef} />
              )
            })
          }
        </List>
      </Box>
      <LMap center={center} zoom={15} ref={mapRef}>
        <TileLayer
          attribution='&amp;copy <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
          url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
        />
        {loading ? null : data.listStores?.items?.map(store => {
          if (!store) {
            return null;
          }
          const detail = stores.find(s => s.uuid === store.id)
          return (
            <Marker
              opacity={detail && detail.isOpen ? 1 : 0.3}
              key={store.id} position={{lat: store.location.latitude, lng: store.location.longitude}}>
              <Popup>
                {store.title}<br/>
                {store.mostDistance.distance}
              </Popup>
            </Marker>
          );
        })}
      </LMap>
    </Grid>
  )
}

export const IndexPage = () => {
  return <IndexPageComponent />
}

export const useOpenStore = () => {
  const [stores, setStore] = useState<StoreDetail[]>([]);
  const findOpenStores = useCallback(async (storeIds: string[]) => {
    const results = await Promise.all(
      storeIds.map(getStore)
    );
    setStore(results)
  }, [])
  return {stores, findOpenStores}
}
