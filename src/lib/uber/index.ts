import axios from "axios";
import {LocationDetail, LocationStoreMap, StoreDetail} from "../../../types/uber_type";
import {CreateStoreInput, MostDistanceInput} from "../../../types/amplify_api";
import {point} from "@turf/helpers";
import distance from "@turf/distance";

export type UberResponse<DataType> = {
  status: "success" | "failure",
  data: DataType
}
export const getAPIpath = (endpoint: string) =>
  (process.env.NODE_ENV === "development" ? "https://cors-anywhere.herokuapp.com/" : "") + `https://www.ubereats.com/api/${endpoint}?localeCode=jp`
export const header = {
  "content-type": "application/json",
  "x-csrf-token": "x",
  "user-agent": "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.116 Safari/537.36",
  "accept-language": "ja,en-US;q=0.9,en;q=0.8,la;q=0.7",
}

export async function getStoresIds (addressEncoded: string): Promise<string[]> {
  const res = await axios.post(getAPIpath("getFeedV1"), null, {
    headers: {
      ...header,
      cookie: `uev2.loc=${addressEncoded}`
    }
  }).catch(e => {
    return e
  })
  if (!res.data || !res.data.data || !res.data.data.storesMap || res.status === 502) {
    return []
  }
  const storesMap = res.data.data.storesMap as object;
  return Object.keys(storesMap)
}


export async function getAddress (landString: string, index: number = 0): Promise<LocationDetail> {
  const autoCompleteRes = await axios.post(getAPIpath("getLocationAutocompleteV1"), {
    query: landString
  }, {
    headers: header
  })
  const firstSuggestion = autoCompleteRes.data.data[0]

  const detailRes = await axios.post<UberResponse<LocationDetail>>(
    getAPIpath("getLocationDetailsV1"),
    firstSuggestion,
    {
      headers: header
    }
  )
  return detailRes.data.data
}

export async function getStore(uuid: string, index: number = 0): Promise<StoreDetail> {
  if (index > 0) {
    await sleep(index * 800);
  }
  console.log(`店舗${index}のサーチ中`)
  const storeRes = await axios.post<UberResponse<StoreDetail>>(getAPIpath("getStoreV1"), {
    storeUuid: uuid,
    sfNuggetCount: 0
  }, {headers: header})
  return storeRes.data.data
}
export function sleep(length: number) {
  return new Promise(resolve => setTimeout(resolve, length));
}

export async function addLocationStoresMap(landString: string, lsMap: LocationStoreMap, index: number = 0): Promise<LocationStoreMap> {
  if (index > 0) {
    await sleep(index * 800);
  }
  console.log(`住所${index}のサーチ中`)
  const locationDetail = await getAddress(landString)
  const addressEncoded = encodeURI(JSON.stringify(locationDetail))

  const storeIds = await getStoresIds(addressEncoded)

  return lsMap.set(locationDetail, storeIds)
}

export async function generateAppsyncStores(storeMaps: Map<string, StoreDetail>, lsMap:  LocationStoreMap) : Promise<CreateStoreInput[]> {
  const storeWithDistArr: CreateStoreInput[] = [];
  storeMaps.forEach((shopDetail, thisStoreKey) => {
    const storePoint = point([shopDetail.location.longitude, shopDetail.location.latitude])
    const mostDistance: MostDistanceInput = {
      place: "",
      distance: 0,
      latitude: 0,
      longitude: 0,
    }
    lsMap.forEach((deliveryStoreKeys, loc) => {
      if (deliveryStoreKeys.find(sk => sk === thisStoreKey)) {
        const locPoint = point([loc.longitude, loc.latitude])
        const dNum = distance(locPoint, storePoint)
        if (dNum > mostDistance.distance) {
          mostDistance.distance = dNum;
          mostDistance.place = loc.address.title;
          mostDistance.longitude = loc.longitude;
          mostDistance.latitude = loc.latitude;
        }
      }
    })
    storeWithDistArr.push(buildAppsyncStore(shopDetail, mostDistance))
    console.log(`${shopDetail.title}と${mostDistance.place}の距離：${mostDistance.distance}` )
  })
  return storeWithDistArr;
}

export function buildAppsyncStore(shopDetail: StoreDetail, mostDistance: MostDistanceInput): CreateStoreInput {
  return {
    id: shopDetail.uuid,
    cityId: shopDetail.cityId,
    location: shopDetail.location,
    phoneNumber: shopDetail.phoneNumber || "",
    priceBucket: shopDetail.priceBucket ? shopDetail.priceBucket : "",
    sanitizedTitle: shopDetail.sanitizedTitle,
    title: shopDetail.title,
    mostDistance: mostDistance,
    rating: shopDetail.rating ? shopDetail.rating : {
      ratingValue: 0,
      reviewCount: "0"
    }
  }
}
