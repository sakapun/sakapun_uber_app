import * as Permissions from 'expo-permissions';
import Constants from 'expo-constants';
import * as Notifications from "expo-notifications";
import {Platform} from "react-native";

// import Amplify, {Auth} from "aws-amplify";
// import awsExports from "../aws-exports";

export async function registerForPushNotificationsAsync(): Promise<string | null> {
  let token = null;
  if (Constants.isDevice) {
    const { status: existingStatus } = await Permissions.getAsync(Permissions.NOTIFICATIONS);
    let finalStatus = existingStatus;
    if (existingStatus !== 'granted') {
      const { status } = await Permissions.askAsync(Permissions.NOTIFICATIONS);
      finalStatus = status;
    }
    if (finalStatus !== 'granted') {
      alert('Failed to get push token for push notification!');
    }
    token = (await Notifications.getExpoPushTokenAsync()).data;
    // console.log(token);
  } else {
    // alert('実機じゃないと通知できないよ！');
  }

  if (Platform.OS === 'android') {
    Notifications.setNotificationChannelAsync('default', {
      name: 'default',
      importance: Notifications.AndroidImportance.MAX,
      vibrationPattern: [0, 250, 250, 250],
      lightColor: '#FF231F7C',
    });
  }

  return token;
}

// const {COGNITO_USER_ID, COGNITO_USER_PW} = process.env
//
// // Setting
// Amplify.configure(awsExports);
// export async function signInNotificationAccount () {
//   // await Auth.signIn(COGNITO_USER_ID || "", COGNITO_USER_PW || "")
// }
